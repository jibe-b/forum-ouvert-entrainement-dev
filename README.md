# Informatique et données libres — session d'apprentissage en pair-à-pair (#1) 

## Pour qui ? Pour toutes celles et ceux intéressé·e·s par 

- les logiciels libres,
- la programmation et la science des données 
- la gestion de données pour l'OpenData quel que soit votre niveau actuel, si vous souhaitez progresser, vous êtes les bienvenu·e·s à des sessions d'entrainement et d'apprentissages en pair-à-pair ! 

## Infos pratiques 

Rdv au .... le ../../.... de ..h.. à ..h.. . 

Amenez un ordinateur, vos questions, vos connaissances (mêmes les plus basiques !) 
et votre motivation :) 

## Qu'est-ce qu'une session d'apprentissage et d'entrainement en pair-à-pair ?

Tous les participant·e·s sont invité·e·s à préparer les questions qu'ils et elles 
se posent et sur lesquelles elles souhaitent progresser. Un ordre du jour sera 
construit en commun en début de session. Tout au long de la séance les participant·e·s 
s'entraident avec les connaissances et compétences des un·e·s et des autres. 

Si la solution à un problème n'est pas connue dans le groupe, 
le groupe cherche des pistes dans des ressources en lignes. 
Lorsque la question demande une aide extérieure, une question 
est posée à un·e expert·e du domaine par email, et une invitation à venir 
contribuer au cours d'une session suivante peut être faite. 

## Pourquoi ces sessions ?

L'informatique, la programmation et l'OpenData sont des thématiques 
qui demandent un apprentissage relativement technique, mais cet apprentissage 
peut avoir lieu dans un contexte relativement souple, sur lequel ces sessions 
se basent. Le choix est fait de ne pas mettre de barrière à l'entrée, 
ni en termes d'argent ni en terme de diplômes. 

## Objectifs de ces sessions 

L'objectif principal est le partage de connaissances et la progression en commun. 

Les objectifs de ces sessions seront ceux que se donneront les participant·e·s. 

Des sous-groupes ayant un ou des objectifs communs peuvent émerger et se structurer, 
tant qu'aucun·e membre ne reste en rade en terme de progression. 

## Public concerné 

Chacune et chacun est bienvenu·e, sans aucune discrimination 
ni contrainte à l'entrée. 

Les participant·e·s s'engagent à faire l'effort d'accompagner les autres 
participant·e·s dans le but de les faire progresser, quelque que soit leur 
niveau de compétence. 

## Règlement 

Aucun comportement machiste, raciste ou 
discriminatoire ne sera toléré. Tout·e participant·e s'y engage. Chaque participant·e 
s'engage à être attentif·ve à faire respecter cette règle. 
L'ensemble des participant·e·s s'engage à ne pas laisser de participant·e à la traine dans les apprentissages. 

## Mise en pratique possible des apprentissages

Différentes mises en pratique des apprentissages sont possibles.

Si les buts coincident, je (jibe-b) vous propose [une option](prestation-a-plusieurs.md)

