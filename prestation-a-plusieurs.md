# Prestation à plusieurs en tant qu'apprenant·e·s

Je (jibe-b) suis contre le cloisonnement entre apprenant·e·s et professionnel·le·s.

Il est indispensable de réaliser des tests pour s'assurer que le code produit
est fonctionnel, quand on est :
- professionnel·le
- apprenant·e

## Préambule (excluant) à ma proposition

Si nos buts coincident (libre, réciprocité, éthique), alors je vous invite
à envisager de fournir des prestations en commun avec moi (jibe-b).

Par défaut, c'est excluant puisque ces critères ne sont pas nécessairement
remplis. (cf point "Précisions sur la notion d'exclusion")

## Proposition

À plusieurs, il est possible de fournir des prestations, contribuer sur des
répos de code libre, etc.

Lançons-nous ensemble, et profitons-en pour y prendre de bons réflexes et 
en apprendre plus !

## Précisions sur la notion d'exclusion

La constitution d'un groupe est un équilibre entre attraction et exclusion.
Dans ce cas, il est plus adapté de choisir de travailler séparément si vous
et moi n'avons pas les mêmes buts. Cela vous évitera de subir ma façon de
fonctionner.